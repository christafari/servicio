
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 10/31/2015 20:09:41
-- Generated from EDMX file: C:\Users\ing christian\Desktop\WcfServiceSa\WcfServiceSa\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BDsa];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Reserva_Agenda]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Reserva] DROP CONSTRAINT [FK_Reserva_Agenda];
GO
IF OBJECT_ID(N'[dbo].[FK_Usuario_Persona]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Usuario] DROP CONSTRAINT [FK_Usuario_Persona];
GO
IF OBJECT_ID(N'[dbo].[FK_Reserva_Sala]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Reserva] DROP CONSTRAINT [FK_Reserva_Sala];
GO
IF OBJECT_ID(N'[dbo].[FK_Usuario_Reserva]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Usuario] DROP CONSTRAINT [FK_Usuario_Reserva];
GO
IF OBJECT_ID(N'[dbo].[FK_Usuario_Rol]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Usuario] DROP CONSTRAINT [FK_Usuario_Rol];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Agenda]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Agenda];
GO
IF OBJECT_ID(N'[dbo].[Persona]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Persona];
GO
IF OBJECT_ID(N'[dbo].[Reserva]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Reserva];
GO
IF OBJECT_ID(N'[dbo].[Rol]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Rol];
GO
IF OBJECT_ID(N'[dbo].[Sala]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sala];
GO
IF OBJECT_ID(N'[dbo].[Usuario]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Usuario];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Agenda'
CREATE TABLE [dbo].[Agenda] (
    [Id_agenda] int  NOT NULL,
    [fecha] datetime  NOT NULL,
    [hora_inicio] time  NOT NULL,
    [hora_fin] time  NOT NULL,
    [Estado_agenda] bit  NOT NULL
);
GO

-- Creating table 'Persona'
CREATE TABLE [dbo].[Persona] (
    [Id_persona] int  NOT NULL,
    [Nombre] varchar(max)  NOT NULL,
    [Apellidos] varchar(max)  NOT NULL,
    [fecha_nacimiento] datetime  NOT NULL,
    [correo] varchar(max)  NOT NULL,
    [celular] nvarchar(10)  NOT NULL
);
GO

-- Creating table 'Reserva'
CREATE TABLE [dbo].[Reserva] (
    [Id_reserva] int  NOT NULL,
    [SalaId] int  NULL,
    [AgendaId] int  NULL
);
GO

-- Creating table 'Rol'
CREATE TABLE [dbo].[Rol] (
    [Id_rol] int  NOT NULL,
    [Tipo] varchar(max)  NULL
);
GO

-- Creating table 'Sala'
CREATE TABLE [dbo].[Sala] (
    [Id_sala] int  NOT NULL,
    [estado] bit  NOT NULL,
    [capacidad] int  NOT NULL,
    [Nombre_sala] varchar(max)  NOT NULL
);
GO

-- Creating table 'Usuario'
CREATE TABLE [dbo].[Usuario] (
    [Id_usuario] int  NOT NULL,
    [Username] varchar(20)  NOT NULL,
    [Password] varchar(max)  NOT NULL,
    [Id_persona] int  NOT NULL,
    [Id_reserva] int  NOT NULL,
    [RolId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id_agenda] in table 'Agenda'
ALTER TABLE [dbo].[Agenda]
ADD CONSTRAINT [PK_Agenda]
    PRIMARY KEY CLUSTERED ([Id_agenda] ASC);
GO

-- Creating primary key on [Id_persona] in table 'Persona'
ALTER TABLE [dbo].[Persona]
ADD CONSTRAINT [PK_Persona]
    PRIMARY KEY CLUSTERED ([Id_persona] ASC);
GO

-- Creating primary key on [Id_reserva] in table 'Reserva'
ALTER TABLE [dbo].[Reserva]
ADD CONSTRAINT [PK_Reserva]
    PRIMARY KEY CLUSTERED ([Id_reserva] ASC);
GO

-- Creating primary key on [Id_rol] in table 'Rol'
ALTER TABLE [dbo].[Rol]
ADD CONSTRAINT [PK_Rol]
    PRIMARY KEY CLUSTERED ([Id_rol] ASC);
GO

-- Creating primary key on [Id_sala] in table 'Sala'
ALTER TABLE [dbo].[Sala]
ADD CONSTRAINT [PK_Sala]
    PRIMARY KEY CLUSTERED ([Id_sala] ASC);
GO

-- Creating primary key on [Id_usuario] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [PK_Usuario]
    PRIMARY KEY CLUSTERED ([Id_usuario] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [AgendaId] in table 'Reserva'
ALTER TABLE [dbo].[Reserva]
ADD CONSTRAINT [FK_Reserva_Agenda]
    FOREIGN KEY ([AgendaId])
    REFERENCES [dbo].[Agenda]
        ([Id_agenda])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Reserva_Agenda'
CREATE INDEX [IX_FK_Reserva_Agenda]
ON [dbo].[Reserva]
    ([AgendaId]);
GO

-- Creating foreign key on [Id_persona] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [FK_Usuario_Persona]
    FOREIGN KEY ([Id_persona])
    REFERENCES [dbo].[Persona]
        ([Id_persona])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Usuario_Persona'
CREATE INDEX [IX_FK_Usuario_Persona]
ON [dbo].[Usuario]
    ([Id_persona]);
GO

-- Creating foreign key on [SalaId] in table 'Reserva'
ALTER TABLE [dbo].[Reserva]
ADD CONSTRAINT [FK_Reserva_Sala]
    FOREIGN KEY ([SalaId])
    REFERENCES [dbo].[Sala]
        ([Id_sala])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Reserva_Sala'
CREATE INDEX [IX_FK_Reserva_Sala]
ON [dbo].[Reserva]
    ([SalaId]);
GO

-- Creating foreign key on [Id_reserva] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [FK_Usuario_Reserva]
    FOREIGN KEY ([Id_reserva])
    REFERENCES [dbo].[Reserva]
        ([Id_reserva])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Usuario_Reserva'
CREATE INDEX [IX_FK_Usuario_Reserva]
ON [dbo].[Usuario]
    ([Id_reserva]);
GO

-- Creating foreign key on [RolId] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [FK_Usuario_Rol]
    FOREIGN KEY ([RolId])
    REFERENCES [dbo].[Rol]
        ([Id_rol])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Usuario_Rol'
CREATE INDEX [IX_FK_Usuario_Rol]
ON [dbo].[Usuario]
    ([RolId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------