﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Serialization;

namespace WcfServiceSa
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        public Model1Container db = new Model1Container();

        public object JsonConvert { get; private set; }

        public Usuario mostrarUsuario(string Id)
        {
            throw new NotImplementedException();
        }

        public List<Usuario> mostrarUsuarios()
        {
            return db.Usuario.ToList();
            
        }
        public bool CrearUsuario(string nombre)
        {
            try
            {
                Usuario usu = new Usuario();
                usu.Username = nombre;

                db.Usuario.Add(usu);
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool EliminarUsuario(string codigo)
        {
            try
            {
                int x = Convert.ToInt32(codigo);
                var con = (from t in db.Usuario where t.Id_usuario == x select t).First();
                db.Usuario.Remove(con);
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public string Login(string username, string pass)
        {
            try
            {
                var st = from usu in db.Usuario
                             where usu.Username == username && usu.Password == pass
                         select new { usu.Username };
                if (st.Any())
                {
                    return JsonConvert.SerializeObject(st, Formatting.None);
                }
                else
                {
                    return "false";
                }
            }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }

        public bool ModificarUsuario(string Id_usuario, string nombre)
        {
            throw new NotImplementedException();
        }

        public Usuario BuscarUsuario(string Id)
        {
            throw new NotImplementedException();
        }

        public string ListarSala()
        {
            throw new NotImplementedException();
        }

        public bool CrearSala(string nombre)
        {
            throw new NotImplementedException();
        }

        public bool EliminarSala(string Id)
        {
            throw new NotImplementedException();
        }

        public bool ModificarSala(string Id, string nombre)
        {
            throw new NotImplementedException();
        }

        public List<Persona> Listarpersona()
        {
            throw new NotImplementedException();
        }

        public bool CrearPersona(string nombre, string descripcion)
        {
            throw new NotImplementedException();
        }

        public bool ModificarTipoUsuario(string codigo, string nombre, string descripcion)
        {
            throw new NotImplementedException();
        }
    }
}
