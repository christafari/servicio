﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceSa
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "mostrarUsuarios", ResponseFormat = WebMessageFormat.Json)]
        List<Usuario> mostrarUsuarios();


        [OperationContract]

        [WebInvoke(Method = "GET", UriTemplate = "mostrarUsuario/{Id}", ResponseFormat = WebMessageFormat.Json)]
        Usuario mostrarUsuario(string Id);


        

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarUsuario/{Id_usuario}", ResponseFormat = WebMessageFormat.Json)]
        bool EliminarUsuario(string Id_usuario);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarUsuario/{codigo}/{nombre}", ResponseFormat = WebMessageFormat.Json)]
        bool ModificarUsuario(string Id_usuario, string nombre);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "BuscarUsuario/{Id}", ResponseFormat = WebMessageFormat.Json)]
        Usuario BuscarUsuario(string Id);
        //============================================================================//
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ListarSala", ResponseFormat = WebMessageFormat.Json)]
        String ListarSala();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CrearSala/{nombre}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool CrearSala(string nombre);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "EliminarSala/{Id}", ResponseFormat = WebMessageFormat.Json)]
        bool EliminarSala(string Id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarSala/{Id}/{nombre}", ResponseFormat = WebMessageFormat.Json)]
        bool ModificarSala(string Id, string nombre);

    
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ListarPersona", ResponseFormat = WebMessageFormat.Json)]
        List<Persona> Listarpersona();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CrearPersona/{nombre}/{Id_persona}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        bool CrearPersona(string nombre, string descripcion);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ModificarPersona/{Id_persona}/{nombre}", ResponseFormat = WebMessageFormat.Json)]
        bool ModificarTipoUsuario(string codigo, string nombre, string descripcion);

       


    }
}
